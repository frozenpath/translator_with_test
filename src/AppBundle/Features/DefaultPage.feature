# language: ru

Функционал: Тестируем добавление фраз на сайт переводчиков и добавление переводов фраз

  Сценарий: Авторизация на сайте и добавление фраз для перевода
    Допустим я нахожусь на главной странице
    И я вижу ссылку авторизации "Login" где-то на странице
    Тогда я выбираю поле "username" и ввожу логин "Frozenpath"
    И я выбираю поле "password" и ввожу логин "123"
    Тогда я нажимаю кнопку "Log in"
    И я добавлю в поле "form_ruName" фразы нажимаю кнопку "Save":
      | phrase                   |
      | моя страница вконтакте   |
      | танки онлайн             |
      | игры для мальчиков       |
      | переводчик гугл          |
      | маша и медведь           |
      | фильмы онлайн            |
      | скачать музыку           |
      | гороскоп на сегодня      |
      | новости украины          |
      | алиэкспресс на русском   |

    Допустим я авторизован и могу нажать "Logout"
    И я перехожу по ссылке и ввожу переводы и жму "Save":
      | link                     | eng                | ger                  | french                    | kgz                    | kaz                          |
      | моя страница вконтакте   | my page vkontakne  | meine Seite vk       | ma page sur VK            | менин бет VK           | менің бет ВКонтакте          |
      | танки онлайн             | the tanks online   | Tanks Online         | réservoirs en ligne       | танктар онлайн         | онлайн резервуарлар          |
      | игры для мальчиков       | games for boys     | Spiele für Jungen    | jeux pour les garçons     | Балдар үчүн оюндар     | ер балаларға арналған ойындар|
      | переводчик гугл          | google translate   | Google Übersetzer    | Traducteur Google         | Google котормочу       | Гугл аудармашы               |
      | маша и медведь           | masha and the bear | Mascha und der Bär   | Macha et l'ours           | Маша жана аюу          | Маша мен аю                  |
      | фильмы онлайн            | films online       | Filme online         | films en ligne            | кино онлайн            | онлайн фильмдер              |
      | скачать музыку           | download music     | Musik kostenlos      | télécharger de la musique | музыка жүктөө          | жүктеу музыка                |
      | гороскоп на сегодня      | horoscope for today| Horoskop für heute   | horoscope pour aujourd'hui| Бүгүнкү күндө          | Бүгінгі таңда жұлдыз         |
      | новости украины          | ukrainian news     | Ukraine News         | Ukraine nouvelles         | Украина кабар          | Украина жаңалықтар           |
      | алиэкспресс на русском   | aliexpress on rus  | aliexpres in Russisch| aliexpress on rus         | Орус тилинде aliexpress| Орыс aliekspress             |