<?php

namespace AppBundle\Features\Context;

use Behat\Behat\Tester\Exception\PendingException;
use Behat\Gherkin\Node\TableNode;
use Behat\MinkExtension\Context\MinkContext;
use Behat\Symfony2Extension\Context\KernelAwareContext;
use Symfony\Component\HttpKernel\KernelInterface;

/**
 * Defines application features from the specific context.
 */
class FeatureContext extends MinkContext implements KernelAwareContext
{
    /** @var  KernelInterface */
    private $kernel;

    /**
     * Initializes context.
     *
     * Every scenario gets its own context instance.
     * You can also pass arbitrary arguments to the
     * context constructor through behat.yml.
     */
    public function __construct()
    {
    }

    protected function getContainer()
    {
        return $this->kernel->getContainer();
    }

    /**
     * Sets Kernel instance.
     *
     * @param KernelInterface $kernel
     */
    public function setKernel(KernelInterface $kernel)
    {
        $this->kernel = $kernel;
    }

    /**
     * @When /^я нахожусь на главной странице$/
     */
    public function яНахожусьНаГлавнойСтранице()
    {
        $this->visit($this->getContainer()->get('router')->generate('app_site_index'));
    }


    /**
     * @When /^я вижу ссылку авторизации "([^"]*)" где\-то на странице$/
     */
    public function яВижуСсылкуАвторизацииГдеТоНаСтранице($arg1)
    {
        $this->clickLink($arg1);

    }

    /**
     * @When /^я выбираю поле "([^"]*)" и ввожу логин "([^"]*)"$/
     */
    public function яВыбираюПолеИВвожуЛогин($arg1, $arg2)
    {
        $this->fillField($arg1, $arg2);
    }

    /**
     * @When /^я нажимаю кнопку "([^"]*)"$/
     */
    public function яНажимаюКнопку($arg1)
    {
        $this->pressButton($arg1);
        sleep(1);
    }

    /**
     * @When /^нажимаю кнопку "([^"]*)"$/
     */
    public function нажимаюКнопку($arg1)
    {
        $this->pressButton($arg1);
        sleep(1);
    }

    /**
     * @When /^я добавлю в поле "([^"]*)" фразу "([^"]*)"$/
     */
    public function яДобавлюВПолеФразу($arg1, $arg2)
    {
        $this->fillField($arg1, $arg2);
    }

    /**
     * @When /^я добавлю в поле "([^"]*)" фразы нажимаю кнопку "([^"]*)":$/
     */
    public function яДобавлюВПолеФразыНажимаюКнопку($arg1, $arg2, TableNode $table)
    {
        foreach ($table as $phrase) {
            $this->fillField($arg1, $phrase['phrase']);
            $this->pressButton($arg2);
            sleep(1);
        }
    }

    /**
     * @When /^я авторизован и могу нажать "([^"]*)"$/
     */
    public function яАвторизованИМогуНажать($arg1)
    {
        $this->assertPageContainsText($arg1);
        sleep(2);
    }

    /**
     * @When /^я перехожу по ссылке и ввожу переводы и жму "([^"]*)":$/
     */
    public function яПерехожуПоСсылкеИВвожуПереводыИЖму($arg1, TableNode $table)
    {
        foreach ($table as $text) {
            $this->clickLink($text['link']);
            sleep(1);
            $this->fillField('form_en', $text['eng']);
            $this->fillField('form_de', $text['ger']);
            $this->fillField('form_fr', $text['french']);
            $this->fillField('form_kg', $text['kgz']);
            $this->fillField('form_kz', $text['kaz']);
            sleep(1);
            $this->pressButton($arg1);
            sleep(2);
            $this->clickLink('Main page');
            sleep(2);
        }
    }

}