<?php

namespace AppBundle\Controller;

use AppBundle\Entity\Phrase;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\HttpFoundation\Request;

class SiteController extends Controller
{
    /**
     * @Route("/")
     * @Method({"POST", "GET"})
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function indexAction(Request $request)
    {
        $user = $this->getUser();

        $form = $this->createFormBuilder()
            ->add('ruName', TextType::class, ['label' => $this->get('translator')->trans('Phrase on Russian')])
            ->add('save', SubmitType::class, ['label' => $this->get('translator')->trans('Save')])
            ->getForm();

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $data = $form->getData();
            $phrase = new Phrase();

            $phrase->translate('ru')->setName($data['ruName']);

            $em = $this->getDoctrine()->getManager();
            $em->persist($phrase);

            $phrase->mergeNewTranslations();
            $em->flush();

            return $this->redirectToRoute('app_site_index');
        }

        $phrases = $this->getDoctrine()->getRepository('AppBundle:Phrase')->findAll();

        return $this->render('AppBundle:Site:index.html.twig', array(
            'user' => $user,
            'form' => $form->createView(),
            'phrases' => $phrases
        ));
    }


    /**
     * @Route("/{_locale}", requirements = {"_locale" : "en|ru"})
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\RedirectResponse
     */

    public function changeLocal(Request $request)
    {
        return $this->redirect($request->headers->get('referer'));
    }


    /**
     * @Route("/show-{phrase_id}")
     * @Method({"POST", "GET"})
     * @param Request $request
     * @param int $phrase_id
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function showAction(Request $request, int $phrase_id)
    {
        $user = $this->getUser();

        $phrase = $this->getDoctrine()
            ->getRepository(Phrase::class)
            ->find($phrase_id);

        $translations = $phrase->getTranslations();
        $have_translation = [];

        /** @var PhraseTranslation $translation */
        foreach ($translations as $translation) {
            if ($translation->getName() !== null && $translation->getLocale() != 'ru') {
                $have_translation[] = $translation->getLocale();
            }

        }

        $builder = $this->createFormBuilder();

        if ($phrase->translate('en')->getName() === null) {
            $builder->add('en', TextType::class, [
                'required' => false]);
        }
        if ($phrase->translate('de')->getName()  === null) {
            $builder->add('de', TextType::class, [
                'required' => false]);
        }
        if ($phrase->translate('fr')->getName()  === null) {
            $builder->add('fr', TextType::class, [
                'required' => false]);
        }
        if ($phrase->translate('kg')->getName()  === null) {
            $builder->add('kg', TextType::class, [
                'required' => false]);
        }
        if ($phrase->translate('kz')->getName()  === null) {
            $builder->add('kz', TextType::class, [
                'required' => false]);
        }

        if (count($have_translation) != 5) {
            $builder->add('save', SubmitType::class, ['label' => $this->get('translator')->trans('Save')]);
        }

        $form = $builder->getForm();
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $data = $form->getData();

            if (isset($data['en'])) {
                $phrase->translate('en')->setName($data['en']);
            }
            if (isset($data['de'])) {
                $phrase->translate('de')->setName($data['de']);
            }
            if (isset($data['fr'])) {
                $phrase->translate('fr')->setName($data['fr']);
            }
            if (isset($data['kg'])) {
                $phrase->translate('kg')->setName($data['kg']);
            }
            if (isset($data['kz'])) {
                $phrase->translate('kz')->setName($data['kz']);
            }

            $em = $this->getDoctrine()->getManager();
            $em->persist($phrase);
            $phrase->mergeNewTranslations();
            $em->flush();
            return $this->redirectToRoute('app_site_show', [
                'phrase_id' => $phrase_id
            ]);
        }

        return $this->render('@App/Site/show.html.twig', array(
            'user' => $user,
            'phrase' => $phrase,
            'form' => $form->createView(),
            'have_translation' => $have_translation
        ));
    }
}
