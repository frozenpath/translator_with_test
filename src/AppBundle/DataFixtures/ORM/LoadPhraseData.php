<?php

namespace AppBundle\DataFixtures\ORM;

use AppBundle\Entity\Phrase;
use Doctrine\Common\DataFixtures\FixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;

class LoadPhraseData implements FixtureInterface
{
    public function load(ObjectManager $manager)
    {
        $phrase1 = new Phrase();
        $phrase2 = new Phrase();
        $phrase3 = new Phrase();

        $phrase1->translate('ru')->setName('Первая фраза на русском языке');
        $phrase2->translate('ru')->setName('Вторая фраза на русском языке');
        $phrase3->translate('ru')->setName('Третья фраза на русском языке');

        $manager->persist($phrase1);
        $manager->persist($phrase2);
        $manager->persist($phrase3);

        $phrase1->translate('en')->setName('The first phrase in Russian');
        $phrase1->translate('de')->setName('Der erste Satz in Russisch');
        $phrase1->translate('fr')->setName('La première phrase en russe');
        $phrase1->translate('kg')->setName('Орус тилинде биринчи жолу сөз');
        $phrase1->translate('kz')->setName('Орыс тілінде бірінші фраза');

        $phrase2->translate('en')->setName('The second phrase in Russian');
        $phrase2->translate('de')->setName('Der zweite Satz in Russisch');
        $phrase2->translate('fr')->setName('La deuxième phrase en russe');


        $phrase1->mergeNewTranslations();
        $phrase2->mergeNewTranslations();
        $phrase3->mergeNewTranslations();

        $manager->flush();
    }
}
